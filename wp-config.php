<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. 12032018 */
define('DB_NAME', 'gentleman_fr');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CI1@b4B~4O@M/hPK09AVVnM$Nf$3]67Z$-b}h*CI5Z|>NWBIClqQNPyH@j!(e`po');
define('SECURE_AUTH_KEY',  '3>3l$ahg!o@_H5vMj*<2-QUw0ue~XDrOo&;?:tUA*Am<AwB2h5~pu7qCfRCiGE^@');
define('LOGGED_IN_KEY',    '~wtB:y}9H}Xa}@%yqLDslIZ&3^EX|u@0j^NN%  (tYg4lvp<7]CW&!gPvf6<WpfY');
define('NONCE_KEY',        'Tj8>x*vs1@/m6{I0XD3!1hlAp5[9Qtve4.+K`JPhX{~|+2eLTn 6.3FaS0Mum+n4');
define('AUTH_SALT',        '1OB9/V1|7}(Hq0VQDJ>CYT&g]|riXt9]Av.fQD$U~%Kdf0z<7ZAY=TBE&>D(7D8U');
define('SECURE_AUTH_SALT', 'bcN)w,,syc$4D+h*yn2Vd`s< yxvOd&V,|6u}(hphI;/CDhRXQtH(g<98>)gABam');
define('LOGGED_IN_SALT',   '{~^@~G8-_`t96F3!=~=#I,HzS!&q6*nHs4Gd{,0=H_(sGcD=NHMZO1fsQ,]OkK~!');
define('NONCE_SALT',       'i_voZa/?cKp<:jL*J7(/p{;^tbFTPig2u,@EiGXw])%%l{&Wh5JnJkd2^_I;z.g{');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_POST_REVISIONS', 8 );

define( 'DISALLOW_FILE_EDIT', true );

define('WP_DEBUG', true);
// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 1 );
// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );
// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings  
define( 'WP_DEBUG_DISPLAY', false );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );

/* That's all, stop editing! Happy blogging. */
define('CONCATENATE_SCRIPTS', false);


/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');