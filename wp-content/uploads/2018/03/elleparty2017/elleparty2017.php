<?php 
/*
Template Name: ELLEPARTY2017
*/
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LOVE x ELLE - ELLE PARTY 2017</title>
        <link rel="stylesheet" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/css/style.min.css">
        <link rel="author" href="humans.txt">
        <!-- Add Fav-Icon -->
        <link rel="apple-touch-icon" sizes="57x57" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta property="fb:app_id" content="455218897878201" />
        <meta property="og:image" content="http://www.elle.be/fr/wp-content/uploads/2017/11/img_elleparty.jpg" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <meta property="og:description" content="De LOVE X ELLE Party 2017, dat is hier! Een onvergetelijke avond lang wordt de Event Lounge omgetoverd tot dé place-to-be van het jaar." />
        <meta property="og:title" content="LOVE x ELLE - ELLE PARTY 2017" />
        <meta property="og:locale" content="nl_NL" />
        
        <!-- Add calendar -->
        <link href="https://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42249957-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-42249957-4');
        </script>
    </head>
    <body>
        <header id="intro">
            <button class="burger">
                <span></span>
            </button>
            <div class="menu-mobile">
                <nav class="menu-main">
                    <ul>
                        <li>
                            <a href="#" class="sprite party-logo">
                            </a>
                        </li>
                        <li>
                            <a href="#about">About</a>
                        </li>
                        <li>
                            <a href="#lineup">Line-Up</a>
                        </li>
                        <li>
                            <a href="#practical">Praktisch</a>
                        </li>
                        <li>
                            <a href="https://www.weezevent.com/love-x-elle/" target="_blank">Koop tickets</a>
                        </li>
                        <li class="no-desktop">
                            <a href="http://www.elle.be/nl/"><span>Terug naar</span>ELLE.be</a>
                        </li>
                        <li class="red">
                            <a href="http://www.elle.be/nl/210821-zo-maak-kans-op-duoticket-elle-party-2017-2.html" class="red" target="_blank">Win tickets</a>
                        </li>
                    </ul>
                </nav>
                <a href="http://www.elle.be/nl/" class="no-mobile"><span>Terug naar</span> ELLE.be</a>
                <nav class="menu-social">
                    <ul>
                        <li class="sprite facebook">
                            <a href="https://www.facebook.com/events/139757710113098/" target="_blank">
                            </a>
                        </li>
                        <li class="sprite instagram">
                            <a href="https://www.instagram.com/explore/tags/elleparty2017/" target="_blank">
                            </a>
                        </li>
                        <!-- <li class="sprite youtube">
                            <a href="" target="_blank">
                            </a>
                        </li> -->
                        <li class="sprite spotify">
                            <a href="https://open.spotify.com/user/ellebelgique/playlist/1TePPhLES8flCSInqv46G9" target="_blank">
                            </a>
                        </li>
                        <li class="sprite pinterest">
                            <a href="https://www.pinterest.fr/ellebelgie/elle-party-2017/" target="_blank">
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="wrap-party">
                <div class="party-logo-love" data-parallax='{"y":130, "x":-3}' ></div>
                <div class="party-logo-elle" data-parallax='{"y":90, "x":-2}'></div>
                <div class="party-girl" data-parallax='{"y":40, "x":-1}'></div>
                <div class="party-gif"></div>
            </div>
            <div class="scrolldown">SCROLL DOWN</div>
        </header>
        <section id="about">
            <div class="btn-red"> 
                <a href="https://www.weezevent.com/love-x-elle/" class="red" target="_blank">Koop tickets</a>
            </div>
            <p class="event-when">
                <span class="showcalendar">
                    <time datetime="2017-12-08">8 december, </time>
                    <time datetime="23:00:00"> om 23u</time>
                    <span class="addtocalendar">
                        <var class="atc_event">
                            <var class="atc_date_start">2017-12-08 23:00:00</var>
                            <var class="atc_date_end">2017-12-09 04:00:00</var>
                            <var class="atc_timezone">Europe/Brussels</var>
                            <var class="atc_title">LOVE x ELLE Party</var>
                            <var class="atc_description">
                                Thema : SAVANNAH | Dress code : Touch of africa | #ElleParty2017 #SavannahParty
                            </var>
                            <var class="atc_location">Event Lounge, Generaal Wahislaan 16/F, 1030 Schaerbeek</var>
                            <var class="atc_organizer">ELLE België | ELLE Belgique</var>
                            <var class="atc_organizer_email">alexia.neefs@editionventures.be</var>
                        </var>
                    </span>
                </span>
                <a href="https://goo.gl/maps/gJiLQBF2a1r" target="_blank" class="address">Event Lounge, Brussels</a>
            </p>
            <p class="event-theme">
                <span>Thema : Savannah</span>
                <span>Dress code : Touch of Africa</span>
            </p>
            <div class="event-intro">
                <p>De <bold>LOVE X ELLE Party 2017</bold>, dat is hier! Een onvergetelijke avond lang wordt de <bold>Event Lounge</bold> omgetoverd tot dé place-to-be van het jaar.</p>
                <p>De <bold> Love x ELLE Party 2017</bold> verenigt iedereen die deel wil uitmaken van het beste feestje van het land in een decor geïnspireerd door de aardse kleuren van de savanne..</p>
                <p>Met zijn <bold>15 edities</bold> is het event uitgegroeid tot een icoon. Dé perfecte locatie kon dus niet ontbreken. Het werd een zoektocht naar de ideale plek om te vieren in stijl. Dat is dé reden waarom dit glamoureuze feest doorgaat aan de rand van Brussel. Op een plek die volledig gewijd is aan feestelijke ontmoetingen en waar onvergetelijke herinneringen zullen worden gecreëerd.</p>
                <p>U kan nu al tickets kopen, maar opgepast, want de plaatsen zijn beperkt. Stel het dus niet uit en koop nu je tickets. De onvergetelijke reis begint op <bold>8 december</bold> vanaf <bold>23 uur</bold> in de <bold>Event Lounge</bold> in Schaarbeek.</p>
            </div>
            <p class="event-hashtags">
                <span>#ELLEParty2017</span>
                <span>#SavannahParty</span>
            </p>
        </section>
        <section id="lineup">
            <h2>Line-Up</h2>
            <ul>
                <li>Fugu Mango Live</li>
                <li>Mr Raoul K</li>
                <li></li>
                <li>Attar!</li>
                <li>Antilope</li>
            </ul>
            <div class="btn-spotify"> 
                <a href="https://open.spotify.com/user/ellebelgique/playlist/1TePPhLES8flCSInqv46G9" class="spotify" target="_blank">Spotify playlist</a>
            </div>
        </section>
        <section id="getready">
            <div class="btn-red"> 
                <a href="https://www.weezevent.com/love-x-elle" class="red" target="_blank">Koop tickets</a>
            </div>
            <h2>HOW TO GET READY LIKE THE ELLE TEAM?</h2>
            <ul>
                <li class="tips-hair">
                    <a href="http://www.elle.be/nl/211185-15-inspirerend-kapsels-elle-party-2017.html" class="btn-to-elle" target="_blank">ZO CREËR JE HET COOLSTE KAPSEL</a>
                </li>
                <li class="tips-ootd">
                    <a href="http://www.elle.be/nl/210813-dresscode-aan-naar-elle-party-2017.html" class="btn-to-elle" target="_blank">SCOOR DE MOOISTE OOTD</a>
                </li>
                <li class="tips-makeup">
                    <a href="http://www.elle.be/nl/211186-kopieer-deze-3-make-up-looks-voor-de-elle-party-2017.html" class="btn-to-elle" target="_blank">ONZE  MAKE-UP TIPS </a>
                </li>
                <li class="tips-contest">
                    <a href="http://www.elle.be/nl/210829-zo-maak-kans-op-duoticket-elle-party-2017.html" class="btn-to-elle" target="_blank">WIN TICKETS</a>
                </li>
            </ul>
            <div class="btn-pint"> 
                    <a href="https://www.pinterest.fr/ellebelgique/elle-party-2017/" class="pint" target="_blank">Onze mood board</a>
                </div>
        </section>
        <section id="practical">
            <h2>PRAKTISCH</h2>
            <p>
                <span>Adres: </span> Event Lounge
            </p>
            <p>
                <span>Generaal Wahislaan 16/F, </span>1030 Schaerbeek</p>
            <div class="btn-googlemaps"> 
                <a href="https://goo.gl/maps/gJiLQBF2a1r" class="googlemaps" target="_blank">OPEN IN GOOGLE MAPS</a>
            </div>
             <div class="map-canvas"></div>
        </section>
        <!-- <section id="getthere">
            <h2>How to get there?</h2>
            <h2>Where to stay after the party?</h2>
        </section> -->
        <section id="partners">
            <h2>Onze partners</h2>
            <ul>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/ford.png" alt="ford logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/ford_h.png" alt="ford logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/babyliss.png" alt="babyliss logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/babyliss_h.png" alt="babyliss logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/baobab.png" alt="baobab logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/baobab_h.png" alt="baobab logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/bombay.png" alt="bombay logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/bombay_h.png" alt="bombay logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/besos.png" alt="besos logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/besos_h.png" alt="besos logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/canon.png" alt="canon logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/canon_h.png" alt="canon logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/carlsberg.png" alt="carlsberg logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/carlsberg_h.png" alt="carlsberg logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/fevertree.png" alt="fevertree logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/fevertree_h.png" alt="fevertree logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/freedent.png" alt="freedent logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/freedent_h.png" alt="freedent logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/gerardbertrand.png" alt="gerardbertrand logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/gerardbertrand_h.png" alt="gerardbertrand logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/godiva.png" alt="godiva logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/godiva_h.png" alt="godiva logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/grisblanc.png" alt="gris blanc logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/grisblanc_h.png" alt="gris blanc logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/kiko.png" alt="kiko logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/kiko_h.png" alt="kiko logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/love.png" alt="love logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/love_h.png" alt="love logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/olivierdachkin.png" alt="olivier dachkin logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/olivierdachkin_h.png" alt="olivier dachkin logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/savoury.png" alt="savoury logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/savoury_h.png" alt="savoury logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/sharingbox.png" alt="sharingbox logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/sharingbox_h.png" alt="sharingbox logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/redbull.png" alt="redbull logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/redbull_h.png" alt="redbull logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/somersby.png" alt="somersby logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/somersby_h.png" alt="somersby logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/tao.png" alt="tao logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/tao_h.png" alt="tao logo">
                </li>
                <li>
                    <img class="partners-1" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/tattinger.png" alt="tattinger logo">
                    <img class="partners-2" src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/img/partners/tattinger_h.png" alt="tattinger logo">
                </li>
            </ul>
        </section>
        <section id="instagram">
            <?php $baseUrl = 'https://www.instagram.com/explore/tags/elleparty2017/?__a=1';
                $json = json_decode(file_get_contents($baseUrl));
                for ($i = 1; $i <= 8; $i++) {
                    echo '<img src="'.$json->tag->media->nodes[$i]->thumbnail_src.'" />';
                } ?>
        </section>
        <footer>
            <ul>
                <li><a href="http://www.elle.be/nl/">ELLE.be</a></li>
                <li>© ELLE Party 2017</li>
            </ul>
        </footer>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAax4X0w-SplnzNSts5ZmJ4xi-ooSliBfk&callback=initMap"></script>
        <script src="https://rawgit.com/atmist/snazzy-info-window/master/dist/snazzy-info-window.min.js"></script>
        <script src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/js/jquery-3.2.1.min.js"></script>
        <script src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/js/jquery.easing.1.3.js"></script>
        <script src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/js/jquery.parallax-scroll.js"></script>
        <script src="http://www.elle.be/nl/wp-content/themes/elle-nl/elleparty2017/assets/js/main.min.js"></script>
        <!-- Add Google Maps -->
        <script>
        $(function() {

            // Create the map
            var map = new google.maps.Map($('.map-canvas')[0], {
                zoom: 16,
                /*styles: mapStyle,*/
                center: new google.maps.LatLng(50.863103,4.3973633),
                disableDefaultUI: true,
                scaleControl: false,
                scrollwheel: false
            });

            var image = 'http://www.elle.be/img/marker.png';
            var beachMarker = new google.maps.Marker({
              position: {lat: 50.863103, lng: 4.3973633},
              map: map,
              icon: image
            });

            // Add a Snazzy Info Window to the marker
            var info = new SnazzyInfoWindow({
                marker: beachMarker,
                placement: 'right',
                offset: {
                    left: '20px'
                },
                content: '<div class="title_event"><b><span class="red">ADRES :</span> EVENT LOUNGE</b></div>' +
                         '<div>Generaal Wahislaan 16/F,<br/> 1030 Schaerbeek</div>'+
                         '<div class="btn-googlemaps"><a target="_blank" href="https://goo.gl/maps/oiU3RQWar1F2">OPEN IN GOOGLE MAPS</a></div>',
                showCloseButton: false,
                closeOnMapClick: false,
                padding: '48px',
                backgroundColor: 'white',
                border: false,
                borderRadius: '0px',
                shadow: false,
                fontColor: '#000',
                fontSize: '15px'
            });
            info.open();
        });
        </script>
        <!-- Add Calendar -->
        <script type="text/javascript">(function () {
                if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
                if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
                    var h = d[g]('body')[0];h.appendChild(s); }})();
        </script>
    </body>
</html>