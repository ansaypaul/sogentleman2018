<?php get_header(); ?>

<main role="main" id="main" class="main main-page">
	<div class="site-inner">
		<div class="content entry-content">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class("h-entry"); ?> data-id = "<?php the_ID(); ?>">
					<header class="entry-header">
						<h1 property="headline">
							<a href="<?php the_permalink(); ?>" class="entry-title" property="mainEntityOfPage" typeof="WebPage">
								<?php the_title(); ?>
							</a>
						</h1>
						<div class="post-details hidden">
							<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php _e( 'Mis à jour le', 'html5blank' ); ?> <?php the_modified_time('j F Y'); ?></span>
							<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
							</span>
							<div property="publisher" typeof="Organization" class="hidden">
								<span property="name"><?php _e( 'So Gentlemen 2018', 'html5blank' ); ?></span>
								<div property="logo" typeof="ImageObject">
								    <!--<link property="url" href="<?php echo home_url(); ?>/img/icons/ellebelgique.png" />
								    <meta property="height" content="125" />
								    <meta property="width" content="320" />-->
								</div>
							</div>
						</div>
					</header>
					<div class="entry-content" property="mainContentOfPage">
						<?php the_content(); ?>
					</div>
					<footer></footer>
				</article>
			<?php endwhile; ?>
			<?php else: ?>
				<article>
					<h2>
						<?php _e( 'Désolé, rien à afficher.', 'html5blank' ); ?>
					</h2>
				</article>
			<?php endif; ?>
		</div>
		<?php get_template_part('sidebar'); ?>
	<div>
</main>

<?php get_footer(); ?>
