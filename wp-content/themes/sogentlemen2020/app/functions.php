<?php
// Gentleman config

require_once('include/helper.php');
require_once('include/gallery.php');


global $member, $actito_bdd, $version;
$version = '1.0.2';

/* ACF options + link file */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(); 
}

function monahans_thumbnail_caption($html, $post_id, $post_thumbnail_id, $size, $attr)
{
    if(is_single()){
      $attachment = get_post($post_thumbnail_id);
      if ($attachment->post_excerpt || $attachment->post_content) {
        $html .= '';
        if ($attachment->post_excerpt) {
          $html .= ''.$attachment->post_excerpt.' ';
        }
      }
    }
  return $html;
}


    add_action('post_thumbnail_html', 'monahans_thumbnail_caption', null, 5);


function elle_get_picture_srcs_nolazyload( $image, $format) {
    $arr = array();
    $mappings = array(
        0 => $format,
        480 => $format,
        640 => $format,
        960 => $format,
        1050 => $format
    );
    foreach ( $mappings as $size => $type ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source srcset="'. $image_src[0] . '" media="(min-width: '. $size .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}

function elle_get_picture_srcs( $image , $format) {
    $arr = array();
    $mappings = array(
        0 => $format,
        480 => $format,
        640 => $format,
        960 => $format,
        1050 => $format
    );
    foreach ( $mappings as $size => $type ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source data-srcset="'. $image_src[0] . '" media="(min-width: '. $size .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}

    //Add custom type Cover
    register_post_type(
      'magazines',
      array(
        'label' => 'Magazines',
        'labels' => array(
          'name' => 'Magazines',
          'singular_name' => 'Magazine',
          'all_items' => __('Tous les magazines', 'html5blank'),
          'add_new_item' => __('Ajouter un magazine', 'html5blank'),
          'edit_item' => __('Éditer le magazine', 'html5blank'),
          'new_item' => __('Nouveau magazine', 'html5blank'),
          'view_item' => __('Voir le magazine', 'html5blank'),
          'search_items' => __('Rechercher parmi les magazines', 'html5blank'),
          'not_found' => __('Pas de magazine trouvé', 'html5blank'),
          'not_found_in_trash'=> __('Pas de magazine dans la corbeille', 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-book',
        'menu_position' => 4,
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );
    
function gentleman_styles( $styles ) {
	$styles->default_version = '20150609';
}
add_action( 'wp_default_styles', 'gentleman_styles' );

function gentleman_setup() {
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'gentleman_setup' );


function gentleman_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'gentleman_excerpt_length', 999 );

function gentleman_excerpt_more( $more ) {
	return '&nbsp;&hellip;&nbsp;&nbsp;[Lire la suite]';
}
add_filter( 'excerpt_more', 'gentleman_excerpt_more' );

function limit_to_three_tags( $terms ) {
	return array_slice( $terms, 0, 3, true );
}
add_filter( 'term_links-post_tag', 'limit_to_three_tags' );

// Popular Posts
function gentleman_set_post_views( $postID ) {
    $count_key = 'gentleman_post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if( $count == '' ) {
        $count = 0;
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '0' );
    }
	else {
        $count++;
        update_post_meta( $postID, $count_key, $count );
    }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

// Featured Image
function gentleman_featured() {
	global $post;
	$already_has_thumb = has_post_thumbnail( $post->ID );
	if ( !$already_has_thumb ) {
		$attached_image = get_children( "post_parent=$post->ID&post_type=attachment&post_mime_type=image&numberposts=1" );
		if ( $attached_image ) {
			foreach ( $attached_image as $attachment_id => $attachment ) {
				set_post_thumbnail( $post->ID, $attachment_id );
			}
		}
	}
}
add_action( 'the_post', 'gentleman_featured' );
add_action( 'save_post', 'gentleman_featured' );
add_action( 'draft_to_publish', 'gentleman_featured' );
add_action( 'new_to_publish', 'gentleman_featured' );
add_action( 'pending_to_publish', 'gentleman_featured' );
add_action( 'future_to_publish', 'gentleman_featured' );

// Top Menu
register_nav_menus(array(
    'topmenu' => 'Top Menu',
	'footermenu' => 'Footer Menu',
	'pagemenu' => 'Page Menu'
));

/**
 * JavaScript
 */
add_action( 'wp_enqueue_scripts', 'gentleman_scripts' );
add_action( 'wp_head', 'gentleman_script_head', 30 );


function load_custom_scripts() {
    wp_deregister_script( 'jquery' );
    wp_register_script('jquery', get_template_directory_uri() . '/js/lib/jquery-2.2.4.min.js', array(), '2.2.4', true); // true will place script in the footer
    wp_enqueue_script( 'jquery' );
}
if(!is_admin()) {
    add_action('wp_enqueue_scripts', 'load_custom_scripts', 99);
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    global $version;
    wp_register_style('html5blank', get_template_directory_uri() . '/css/styles.css', array(), $version, 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!

    wp_register_style('swiper', get_template_directory_uri() . '/css/swiper.min.css', array(), $version, 'all');
    wp_enqueue_style('swiper'); // Enqueue it!
}

add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet

function gentleman_scripts() {
	global $version;
	    
		wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1', true); // Modernizr
	    wp_enqueue_script('modernizr');

	    wp_register_script( 'showads', get_template_directory_uri() . '/js/show_ads.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script('showads');

        wp_register_script('postscribe', get_template_directory_uri() . '/js/lib/postscribe.js', array('jquery'), '1.0.0', true); // picturefill
        wp_enqueue_script('postscribe');

        wp_register_script('swiper', get_template_directory_uri() . '/js/lib/swiper.min.js', array('jquery'), '3.4.5', true); // Custom scripts
        wp_enqueue_script('swiper');

         wp_register_script('scrollbar', get_template_directory_uri() . '/js/lib/jquery.mCustomScrollbar.concat.min.js', array('jquery'), $version, true); // Custom Scrollbar
        wp_enqueue_script('scrollbar');

        wp_register_script('mustache', get_template_directory_uri() . '/js/lib/mustache.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('mustache');

        wp_register_script('fotterscript', get_template_directory_uri() . '/js/footer-script.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('fotterscript');

        wp_register_script( 'fontfaceobserver', get_template_directory_uri() . '/js/lib/fontfaceobserver.js', array( 'fotterscript' ), $version,true);  
        wp_enqueue_script('fontfaceobserver');  

        /*** GALLERY ***/
        wp_register_script( 'masonry',   get_template_directory_uri() . '/js/lib/masonry.pkgd.min.js', array( 'fotterscript' ), $version,true);
        wp_enqueue_script('masonry');
        wp_register_script( 'fancybox', get_template_directory_uri() . '/js/lib/jquery.fancybox.min.js', array( 'fotterscript' ), $version,true);  
        wp_enqueue_script('fancybox');  

}

function gentleman_script_head() {
	$ad_position = 'others'; if ( is_home() && is_front_page() ) { $ad_position = 'homepage'; }
 ?>

<?php if(get_field('language','option') == 'fr' ){ ?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-53946781-3', 'gentleman.be');
	ga('create', 'UA-53946781-1', {'name':'fr'});

	ga('require', 'displayfeatures');
	ga('fr.require', 'displayfeatures');

	ga('send', 'pageview');
	ga('fr.send', 'pageview');
</script>
<?php } else { ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-53946781-3', 'gentleman.be');
    ga('create', 'UA-53946781-2', {'name':'nl'});

    ga('require', 'displayfeatures');
    ga('nl.require', 'displayfeatures');

    ga('send', 'pageview');
    ga('nl.send', 'pageview');
</script>
<?php } ?>

<script>
	function setGentlekie( e, t, n ) {
		var r = new Date(),
			i = new Date();
		i.setTime( r.getTime() + n * 24 * 60 * 60 * 1e3 );
		document.cookie = e + '=' + encodeURIComponent( t ) + ';expires=' + i.toGMTString() + ';domain=.so-gentleman.be;path=/';
	}
	function getGentlekie( e ) {
		var t = document.cookie,
			n, r, i;
		e = e + '=';
		for ( r = 0, c = t.length; r < c; r++ ) {
			i = r + e.length;
			if ( t.substring( r, i ) == e ) {
				cookEnd = t.indexOf( ';', i );
				if ( cookEnd == -1 ) {
					cookEnd = t.length;
				}
				return decodeURIComponent( t.substring( i, cookEnd ) );
			}
		}
		return null;
	}

	var adPosition = '<?php echo $ad_position; ?>',
		height = document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight,
		width = document.documentElement.clientWidth || window.innerWidth || document.body.clientWidth;
</script>

<?php
}


/**
 *  Guest Preview
 */
add_filter( 'page_row_actions', 'baw_post_row_actions', 10, 2 );
add_filter( 'post_row_actions', 'baw_post_row_actions', 10, 2 );
add_filter( 'posts_results', 'baw_posts_results' );

function baw_post_row_actions( $actions, $post ) {
	$nonce = substr( wp_hash( ceil( time() / ( 7200 ) ) . __FILE__ . $post->ID, 'nonce' ), -12, 10 );
	$url = add_query_arg( array( 'p' => $post->ID, 'preview' => true, '_previewnonce' => $nonce ), home_url( '/?p=' . $post->ID ) );
	$actions[ 'quest_preview' ] = '<a href="'. $url .'">Guest Preview</a>';
	return $actions;
}

function baw_posts_results( $posts ) {
	if ( !empty( $posts ) && isset( $_GET[ 'preview' ], $_GET[ 'p' ], $_GET[ '_previewnonce' ] ) )
		if ( $_GET[ '_previewnonce' ] == substr( wp_hash( ceil( time() / ( 7200 ) ) . __FILE__ . $_GET[ 'p' ], 'nonce' ), -12, 10 ) )
			$posts[0]->post_status = 'publish';
		else wp_die( __( "Il n'y a pas de contenu pour le moment." ) );
	return $posts;
}

/**
 * TinyMCE
 */
add_filter( 'mce_buttons', 'gentleman_editor' );

function gentleman_editor( $mce_buttons ) {
    $pos = array_search( 'wp_more', $mce_buttons, true );
    if ( $pos !== false ) {
        $tmp_buttons = array_slice( $mce_buttons, 0, $pos+1 );
        $tmp_buttons[] = 'wp_page';
        $mce_buttons = array_merge( $tmp_buttons, array_slice( $mce_buttons, $pos+1 ) );
    }
    return $mce_buttons;
}

// Elodie
register_nav_menus( array(
	'top-menu-2018' => 'top-menu-2018',
	'main-menu-2018' => 'main-menu-2018',
	'magazines-menu-2018' => 'magazines-menu-2018',
) );
function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'firstarticle-2018', 625, 460, true );
    add_image_size( 'secondarticle-2018', 420, 310, true );
    add_image_size( 'relatedarticle-2018', 245, 180, true );
    add_image_size( 'magcover-2018', 400, 9999, false );
    add_image_size( 'gallery2018', 9999, 400, false );
    add_image_size( 'gallery2018mid', 9999, 1000, false );
    
    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}
// Breadcrumbs
function custom_breadcrumbs() {
       
    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Homepage';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}
/* CUSTOM POST TYPE */
register_post_type(
  'contact',
  array(
    'label' => 'Contacts',
    'labels' => array(
      'name' => 'Contacts',
      'singular_name' => 'Contact',
      'all_items' => 'Tous les contacts',
      'add_new_item' => 'Ajouter un contact',
      'edit_item' => 'Éditer le contact',
      'new_item' => 'Nouveau contact',
      'view_item' => 'Voir le contact',
      'search_items' => 'Rechercher parmi les contacts',
      'not_found' => 'Pas de contact trouvé',
      'not_found_in_trash'=> 'Pas de contact dans la corbeille'
      ),
    'public' => true,
    'capability_type' => 'post',
    'menu_icon'   => 'dashicons-groups',
    'menu_position' => 5,
    'supports' => array(
      'title',
      'thumbnail'
    ),
    'has_archive' => true
  )
);

//Add custom type Boutiques
register_post_type(
  'Boutiques',
  array(
    'label' => 'Boutiques',
    'labels' => array(
      'name' => 'Boutiques',
      'singular_name' => 'Boutique',
      'all_items' => __('Toutes les boutiques', 'html5blank'),
      'add_new_item' => __('Ajouter une boutique', 'html5blank'),
      'edit_item' => __('Éditer la boutique', 'html5blank'),
      'new_item' => __('Nouvelle boutique', 'html5blank'),
      'view_item' => __('Voir la boutique', 'html5blank'),
      'search_items' => __('Rechercher parmi les boutiques', 'html5blank'),
      'not_found' => __('Pas de boutiques trouvées', 'html5blank'),
      'not_found_in_trash'=> __('Pas de boutiques dans la corbeille', 'html5blank')
      ),
    'public' => true,
    'capability_type' => 'post',
    'menu_icon'   => 'dashicons-store',
    'menu_position' => 6,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail'
    ),
    'has_archive' => true
  )
);