<!-- Contest -->
<?php $posts_contest = get_posts(array ( 'posts_per_page' => 1, 'category_name' => 'concours', 'ignore_sticky_posts' => 1, 'orderby' => 'rand')); ?>
<?php if ($posts_contest)  { ?>
		<?php foreach ( $posts_contest as $post ) : setup_postdata( $post ); ?>
			<h3 class="title-contest"><?php _e( 'Concours', 'html5blank' ); ?></h3>
			<div class="contest">
				<a href="<?php the_permalink(); ?>" class="post-thumbnail">
					<?php the_post_thumbnail('secondarticle-2018'); ?>
				</a>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		<?php endforeach; wp_reset_postdata(); ?>
<?php } else { ?>
		<h3><?php _e( 'Concours', 'html5blank' ); ?></h3>
		<div class="contest">
			<p><?php _e( "Il n'y a pas de concours actuellement", 'html5blank' ); ?>.</p>
		</div>
<?php } ?>  
