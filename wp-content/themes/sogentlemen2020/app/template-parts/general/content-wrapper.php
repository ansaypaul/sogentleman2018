<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[0];

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
?>

<div class="content entry-content">
	<article id="post-<?php the_ID(); ?>">
		<div vocab="http://schema.org/" typeof="Article">
			<header class="entry-header">
				<h1 property="headline">
					<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
				</h1>
				<!-- post-details -->
				<div class="post-details">
					<span class="date">	
						<?php $str_update =  __( 'Publié le', 'html5blank' ); ?>
						<?php if(get_the_modified_time('Y-m-j') !=  get_the_time('Y-m-j')){ $str_update = __( 'Mis à jour le', 'html5blank' ); } ?>
							<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php echo $str_update; ?> <?php the_modified_time('j F Y'); ?></span>
							<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
							</span>
					</span>
					<div property="publisher" typeof="Organization" class="hidden">
						<span property="name"><?php _e( 'So Gentleman', 'html5blank' ); ?></span>
					</div>
				</div>
				<!-- post-thumbnail -->
				<?php if(has_post_thumbnail() && $display_thumb ){ // Check if Thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail"> da
						<?php the_post_thumbnail('firstarticle-2018',array('',0,1,1)); ?>
					</a>
					<a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a>
				<?php } ?>
			</header>
			<div class="entry-content e-content" property="articleBody">
				<?php echo apply_filters( 'the_content', $post->post_content );  ?>
			</div>
			<footer class="entry-footer">
			</footer>
		</div>
	</article>
</div>

<?php get_template_part('template-parts/general/article-related-bottom');  ?>

