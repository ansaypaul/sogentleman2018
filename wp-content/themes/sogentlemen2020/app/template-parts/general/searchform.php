<form method="get" action="http://www.so-gentlemen.be/<?php echo get_field('language','option'); ?>/"class="search-form" role="search">
	<div class="site-inner">
		<div class="search-input-wrapper">
			<label for="s"><?php _e('Recherche', 'html5blank' ); ?>
				<input autocomplete="off" x-webkit-speech="true" class="input_ajax search-input" type="text" name="s" placeholder="<?php _e('Rechercher un article', 'html5blank' ); ?>">
				<span class="search-icon"></span>
			</label>
		</div>
		<input type="hidden" class="category-search-id" value=""/>
		<input class="search-submit" value="<?php _e('Rechercher', 'html5blank' ); ?>" type="submit">
	</div>
</form>