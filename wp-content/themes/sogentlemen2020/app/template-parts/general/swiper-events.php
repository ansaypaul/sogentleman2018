<?php global $post; ?>
<?php  
	$category_main = get_field('categorie_magazine');
	$category_event = get_field('categorie_event','option')
 ?>
<?php $posts = get_posts(array('posts_per_page' => 6, 'category__and' => array( $category_main, $category_event ))); ?>
<?php if($posts){ ?>
<h2><?php _e( 'Événements'); ?></h2>
	<div class="swiper-article-block event">
		<div class="swiper-article swiper-article">
			<div class="swiper-container-video">
					<div class="swiper-wrapper">
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
								<div class="swiper-slide">
									<div class="swiper-image">
										<a href="<?php the_permalink($post->ID); ?>">
											<?php the_post_thumbnail('secondarticle-2018'); ?>
										</a>
										<a href="<?php the_permalink($post->ID); ?>">
											<?php the_title(); ?>
										</a>
									</div>
								</div>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="swiper-button-prev swiper-button-prev-article sprite"><span></span></div>
		<div class="swiper-button-next swiper-button-next-article sprite"><span></span></div>
		<div class="swiper-pagination swiper-pagination-insta"></div>

	</div> 
<?php } ?>