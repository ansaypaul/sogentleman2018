<?php global $excluded_ID; ?>
		<?php 
			$magazine_array = ['Lobby','Gentleman','Play Golf','Play Tennis','Zoute Paper','High Life','Club Grand Place','Event'];
			$list_categorie = get_the_category(); 
			$main_categorie = $list_categorie[0];
			$excluded_ID[] = $post->ID; 
			if(in_array($main_categorie->name, $magazine_array)){
				$main_categorie = $list_categorie[1];
			}

		?> 
	<article class="second-article left">
		<div class="post-side-left">
			<a href="<?php the_permalink(); ?>" class="post-thumbnail">
				<?php the_post_thumbnail('secondarticle-2018'); ?>
			</a>
		</div>
		<div class="post-side-right">
			<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category">
				<?php echo $main_categorie->name; ?>
			</a>
			<div class="post-side-block">
				<h2>
		        	<a href="<?php the_permalink();?>" class="post-title"><?php the_title(); ?></a>
		        </h2>
	    	</div>
	    </div>
	</article>