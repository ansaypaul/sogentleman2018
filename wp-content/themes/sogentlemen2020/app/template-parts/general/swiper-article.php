<?php global $post; ?>
<?php $posts = get_posts(array('post_type'=> 'magazines','posts_per_page' => -1 )); ?>
<?php if($posts){ ?>
	<div class="swiper-article-block">
		<div class="swiper-article swiper-article">
			<div class="swiper-container-article">
					<div class="swiper-wrapper">
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
								<div class="swiper-slide">
									<div class="swiper-image">
										<a href="<?php the_permalink($post->ID); ?>">
											<img alt="" src="<?php echo get_field('visuel_magazine', $post->ID); ?>">
										</a>
									</div>
								</div>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="swiper-pagination swiper-pagination-insta"></div>
	</div> 
<?php } ?>