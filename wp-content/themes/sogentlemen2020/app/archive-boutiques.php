<?php get_header(); ?>
<?php if(isset($_POST)){
		$id_magazine = $_POST['id_magazine'];
	}else{
		$id_magazine = '';
	}
?>
<main role="main" id="main" class="main main-boutiques">
	<div class="site-inner">
		<div class="content entry-content">
			<h1>
				<?php _e( 'Où trouver nos titres', 'html5blank' ); ?>
			</h1>
			<form method="POST">
				<select name="id_magazine">
					<option value=""><?php _e('Tous les magazines', 'html5blank'); ?></option>
					<?php 
					$magazines = get_posts(array( 'post_type' => 'magazines', 'orderby'=>'ID', 'order' => 'ASC', 'posts_per_page' => -1)); 
					if( $magazines ){ 
					foreach( $magazines as $magazine ) : setup_postdata( $post );  ?>
						<option <?php if($magazine->ID == $id_magazine){echo 'selected';} ?> value="<?php echo $magazine->ID ?>"><?php echo $magazine->post_title ?></option>
					<?php	
	 				endforeach; wp_reset_postdata();
	 				}
					?>
				</select>
				<input type="submit" value="<?php _e('Rechercher', 'html5blank'); ?>"><?php _e('Rechercher', 'html5blank'); ?></input>
			</form>
			<?php
			if(isset($_POST['id_magazine'])){ 
			$boutiques = get_posts(array( 'post_type' => 'boutiques', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1, 
				'meta_query' => array(
			       array(
			           'key' => 'shop_mag',
			           'value' => $_POST['id_magazine'] ,
			           'compare' => 'like',
			       ) 
			     )))
			; 
			if( $boutiques ){ 
				foreach( $boutiques as $post ) : setup_postdata( $post ); 
					$first_letter = strtoupper(substr($post->post_title,0,1));
      				if((isset($_GET['l']) && ($first_letter == strtoupper($_GET['l']) || $_GET['l'] == "TOUTES")) || !isset($_GET['l'])) { 
						$shop_address = get_field( 'shop_address' ); 
						$shop_telephone = get_field( 'shop_telephone' ); 
						$shop_url = get_field( 'shop_url' ); 
						$shop_mag = get_field( 'shop_mag' ); 
						?>
						<div class="shop-item">
							<h3>
								<?php the_title(); ?>
							</h3>
							 <?php  if( $shop_address ) { ?>
								<p class="shop-address"><?php echo $shop_address; ?></p>
							<?php } ?>
							<?php  if( $shop_url ) { ?>
								<a href="<?php echo $shop_url; ?>" class="shop-url" target="_blank"><?php echo $shop_url; ?></a>
							<?php } ?>
							<?php  if( $shop_telephone ) { ?>
								<a href="tel:<?php echo $shop_telephone; ?>" class="shop-tel"><?php echo $shop_telephone; ?></a>
							<?php } ?>
						</div>
						<?php } ?>
					<?php endforeach; wp_reset_postdata(); ?>
			<?php } 
			}
			?>
		</div>
		<?php get_template_part('sidebar'); ?>
	<div>
</main>

<?php get_footer(); ?>