<?php get_header(); ?>
<?php $excluded_ID  = array(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" id="main" class="main main-index">
	<!-- site-inner -->
	<div class="site-inner">
		<!-- NOS TITRES -->
		<div class="nos-titres">
			<span class="title-h1">
				<span><?php _e( 'Nos titres', 'html5blank' ); ?></span>
			</span>
			<?php 
				$mazagine_tops = get_field('magazine_index','option');
				foreach ($mazagine_tops as $mazagine_top) { ?>
					<a href="<?php echo get_the_permalink($mazagine_top->ID); ?>" class="nos-titres-magazines">
						<?php echo get_the_title($mazagine_top->ID); ?>
						<?php echo get_the_post_thumbnail($mazagine_top->ID); ?>
					</a>
			<?php } ?>
		</div>
	</div>
	<div class="site-inner">
		<div class="articles content entry-content">
			<div class="nos-articles">
				<span class="title-h1">
					<span><?php _e( 'Nos articles', 'html5blank' ); ?></span>
				</span>
				<?php 
					$args = array('posts_per_page'=> 9 , 'post__not_in' => $excluded_ID, 'paged' => $paged);
					$posts = get_posts($args); 
					$mazagine_tops = get_field('magazine_index','option'); 
					foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
						get_template_part('template-parts/general/article-block-2020'); 
					endforeach; wp_reset_postdata(); 
				?>
			</div>
		
			<!-- <?php $categorie_concours = get_field('categorie_concours','option'); ?>
			<div class="nos-concours">
				<span class="title-h1">
					<span><?php _e( 'Nos concours', 'html5blank' ); ?></span>
				</span>
				<?php 
					$args = array('posts_per_page'=> 3 , 'category' => $categorie_concours, 'post__not_in' => $excluded_ID, 'paged' => $paged);
					$posts = get_posts($args); 
					$mazagine_tops = get_field('magazine_index','option'); 
					foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
						get_template_part('template-parts/general/article-block-2020'); 
					endforeach; wp_reset_postdata(); 
				?>
			</div> -->
			<div class="navigation"><?php posts_nav_link( '  ', __('Publications récentes','html5blank') ,  __('Anciennes publications','html5blank') ); ?></div>

			<?php $categorie_event = get_field('categorie_event','option'); ?>
			<div class="nos-events">
				<span class="title-h1">
					<span><?php _e( 'Nos événements', 'html5blank' ); ?></span>
				</span>
				<?php 
					$args = array('posts_per_page'=> 3 , 'category' => $categorie_event, 'post__not_in' => $excluded_ID, 'paged' => $paged);
					$posts = get_posts($args); 
					$mazagine_tops = get_field('magazine_index','option'); 
					foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
						get_template_part('template-parts/general/article-block-2020'); 
					endforeach; wp_reset_postdata(); 
				?>
			</div>
			<div class="navigation"><a href="<?php echo __('http://www.so-gentlemen.be/fr/event','html5blank'); ?>"><?php echo __('Tous les évènements','html5blank'); ?></a></div>
		<!--
		<?php if($paged == 1){ get_template_part('template-parts/general/first-article'); } ?>
		<div class="articles content entry-content">
		<?php get_template_part('template-parts/general/swiper-article'); ?>
				<?php 
			    	$args = array('posts_per_page'=> 10 , 'post__not_in' => $excluded_ID, 'paged' => $paged);
					$posts = get_posts($args); 
					$current_post = 0;
					foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
						if ($current_post % 2 == 0): 
					        get_template_part('template-parts/general/article-left'); 
					    else: 
					        get_template_part('template-parts/general/article-right'); 
					    endif ?>
						<?php if ($current_post == 1){ ?> <?php } ?>
						<?php if ($current_post == 3){ ?><?php } ?> 
						<?php if ($current_post == 8){ ?><?php } ?> 
				<?php endforeach; wp_reset_postdata(); ?>
			-->
		</div>
		<?php get_template_part('sidebar'); ?>
	</div>
</main>
<?php get_footer(); ?>