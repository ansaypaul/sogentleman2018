<?php get_header(); ?>
<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
if(count($list_categorie) > 1){
	$sous_categorie = $list_categorie[1];
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
?>
<main role="main" id="main" class="main main-single main-magazines">
	<div class="site-inner">
		<?php while (have_posts()) : the_post(); setup_postdata($post); ?>
			<div class="content entry-content">
				<article id="post-<?php the_ID(); ?>">
					<header class="entry-header">
						<?php if(has_post_thumbnail() && $display_thumb ){ ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
								<?php the_post_thumbnail('magcover-2018',array('',0,1,1)); ?>
							</a>
						<?php } ?>
						<h1 property="headline">
							<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
						</h1>
						<div>
							<?php get_template_part('template-parts/general/swiper-article-magazine');  ?>
						</div>
						<?php $liste_plaquette = get_field( "liste_plaquette" ); ?>
						<?php 
						foreach ($liste_plaquette as $plaquette){ ?>
    						<a href="<?php echo $plaquette['plaquette_single']; ?>" target="_blank" class="plaquette"><?php  echo $plaquette['plaquette_titre']; ?></a>
						<?php } ?>
					</header>
					<div class="entry-content e-content" property="articleBody">
						<?php echo apply_filters( 'the_content', $post->post_content );  ?>
						<div>
							<?php 
							get_template_part('template-parts/general/swiper-events'); ?>
						</div>
						<?php $contacts = get_field( "contacts_jetlag" ); ?>
						<?php  if( $contacts ) { ?>
							<h2><?php _e( 'Contacts'); ?></h2>
							<div class="contact-wrapper">
							<?php global $excluded_ID; $i=0; 
							 	foreach ( $contacts as $ad ) : setup_postdata( $ad ); $i++; ?>
							 		<div class="contact-card">
							 			<?php 
							 				$email = get_field( "e-mail", $ad->ID );
							 				$fonction = get_field( "fonction",  $ad->ID );
							 				$gsm = get_field( "numero_de_gsm", $ad->ID );
							 				$fixe = get_field( "numero_de_fixe", $ad->ID );
							 				if( $email) { echo "<a class='contact-email' href='mailto:".$email."'></a>"; }
							 				echo "<a class='contact-name' href='mailto:".$email."'>".get_the_title($ad->ID)."</a>";
							 				if( $fonction ) { echo "<div class='contact-fonction'>".$fonction."</div>"; }
								 			if( $gsm) { echo "<a class='contact-gsm' href='tel:".$gsm."'>".$gsm."</a>"; }
								 			if( $fixe ) { echo "<a class='contact-fixe' href='tel:".$fixe."'></a>"; }
							 			?>
							 		</div>
							 	<?php endforeach; wp_reset_postdata(); ?>
							</div>
						<?php } ?>
					</div>
					<footer class="entry-footer"></footer>
				</article>
			</div>
		<?php endwhile; ?>
		<?php get_template_part('sidebar'); ?>
	</div>
</main>
<?php get_footer(); ?>
