(function ($, root, undefined) {
	var path_windows = window.location.href;
	var arr = path_windows.split("/");
	var elle_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];
	/* <![CDATA[ */
	var ajax_search = {"ajaxurl": elle_path + "/wp-admin/admin-ajax.php"};
	/* ]]> */

	function check_image_a_la_une(){
			if($('#_thumbnail_id').val() != -1){
				$('#publish').prop('disabled', false);
				$('#publish').attr('title', '');
			}else{
				$('#publish').prop('disabled', true);
				$('#publish').attr('title', 'Feature image is mandatory to publish !');
			}
	}

	function ajax_do_instagram_load(){
		
				console.log('Do ajax_do_instagram_load !');
					$.ajax({
						url: ajaxurl,
						type: 'post',
						data: { action: 'erase_uploaded_images', url_instagram: $('#acf-field_5a328a89c61c1').val()},
					
					beforeSend: function() {
						var loading = $('<img style="margin-left:20px" id="loading_gif">');
						loading.attr('src', 'https://www.elle.be/fr/wp-content/themes/ellev2017/img/ajax-loader.gif');
						loading.insertAfter('#field_5a328a89c61c1-button')
							},
					success: function( html ) {
						data = JSON.parse(html);
						$('#acf-field_5a328ac6992e1').val(data['thumbnail_url']);
						$('#acf-field_5a33903d12e75').val(data['author_name']);
						$('#acf-field_5a33927f003a6').text(data['title']);
						
						/*s = data['title'];
						var re = /(?:^|\W)#(\w+)(?!\w)/g, match, matches = [];
						while (match = re.exec(s)) {
						  matches.push(match[1]);
						}
						alert(matches);*/

						$('#_thumbnail_id').val(data['insta_thumb_img_id']);
						$('#apercu_instagram').remove();
						$('#set-post-thumbnail').remove();
						$('#set-post-thumbnail-desc').remove();
						$('#postimagediv .hide-if-no-js').remove();
						$('#loading_gif').remove();
						var img = $('<img id="apercu_instagram">');
						img.attr('src', data['thumbnail_url']);
						img.attr('width', 300);
						img.appendTo('#postimagediv .inside');
						$('html body').fadeTo( "slow", 1 );
							},
					error: function (xhr) {
						alert('Erreur loading instagram !'); 
				   	} 
		  		});
			};


	$(document).ready(function() {

		$('.acf-field-5a328ac6992e1').hide();
		var input = $('<input style="margin-top:10px" class="wp-core-ui button-primary" id="field_5a328a89c61c1-button" type="button" value="Télécharger le post instagram" />');
		input.appendTo($('.acf-field-5a328a89c61c1'));
		$('#field_5a328a89c61c1-button').click(function() { ajax_do_instagram_load(); });
		check_image_a_la_une();
		setInterval(check_image_a_la_une, 800);
		$('.size').hide();
	});


})(jQuery, this);