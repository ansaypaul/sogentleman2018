<?php
function wp_get_attachment( $attachment_id ) {
      $attachment = get_post( $attachment_id );
      if(isset($attachment)){
        $args = array(
              'number_index' => '',
              'id' => $attachment->ID,
              'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
              'caption' => $attachment->post_excerpt,
              'description' => $attachment->post_content,
              'href' => get_permalink( $attachment->ID ),
              'src' => $attachment->guid,
              'title' => $attachment->post_title,
              'desc_rich' => get_field('desc_rich', $attachment->ID ),
              'shop_link' => get_post_meta( $attachment->ID, 'shop_link', true ),
              'shop_price' => get_post_meta( $attachment->ID, 'shop_price', true ),
              'gallery_link_url' => get_post_meta( $attachment->ID, '_gallery_link_url', true ),
              'gallery_pic-thumb' => wp_get_attachment_image_src( $attachment->ID, 'gallery2018mid' )[0],
              'gallery_pic' => wp_get_attachment_image_src( $attachment->ID, 'gallery2018' )[0],
              'slug'=> basename(get_permalink( $attachment->ID )),
              'srcset'=> elle_get_picture_srcs_nolazyload($attachment->ID,'gallery2018'),
              'cta_title'=> get_post_meta( $attachment->ID, 'cta_title', true ),
          );
        
        if($args['desc_rich'] == ''){ $args['desc_rich'] = $args['caption']; }
        return $args;
        }
    }


     // Galeries
    function wptester_post_gallery($output, $attr) {
        global $post, $wp_locale, $idgallery;

        $attr['columns'] = (isset($attr['columns']) && !empty($attr['columns'])) ? $attr['columns']  : 0;
        if($attr['columns'] == null){ $attr['columns'] = 3; }
        $attr['ds_firstpic'] = (isset($attr['ds_firstpic']) && !empty($attr['ds_firstpic'])) ? $attr['ds_firstpic'] : array();
        $ids = explode( ',', $attr['ids'] );
        $output = '';
        $i = 0;
        setup_postdata( $post );
        $idgallery++;

        if(is_page_template( 'template-amp-2.php' )){  
            $output = '<h2 class="h3 mb1">Gallerie</h2><amp-carousel width="500" height="550" layout="responsive" type="slides" class="mb4">';
            while ($i <= $attr['ds_number_image']) {
            $pic = $this->wp_get_attachment($ids[$i]);
              $output .= '
              <figure class="ampstart-image-with-caption m0 relative mb4">
                <amp-img src="'.$pic['gallery_pic-thumb'].'" width="500" height="500" alt="" layout="responsive" class=""></amp-img>
                <figcaption class="h5 mt1 px3">'.$pic['caption'].'</figcaption>
              </figure>';
               $i++;
            }
            $output .= '</amp-carousel>';
            return $output;
            //break;
        }


    if(!is_page_template( 'template-amp-2.php' )){
        $template = "";
        if(array_key_exists('ds_template',$attr)){ $template =  $attr['ds_template']; }

            $i = 0;$slider = 0;$row = 0;
            while ($i < count($ids)) {
                    $picture = wp_get_attachment($ids[$i]);
                    $picture['number_index'] = $i + 1;
                    $data[] = $picture;
                    $i++;
            }

           
            $data_slider = array_chunk($data, 9);
            $number_array = count($data_slider) -1;
            for($i = 0; $i <= $number_array; $i++) {
                $data_slider['picture'][$i] = array_chunk($data_slider[$i], 3);
            }

            $name_gallery = $post->ID .'_'.$idgallery;
            $data_slider['id_gallery'] = $name_gallery;
            $output .= '<div data-id="'.$name_gallery.'" class="gallery_wrapper '.$template.'" id="'.$name_gallery.'">';
            $output .= "<script>var gallery_ajax_".$name_gallery." =".json_encode($data_slider).";</script>";
            $output .= '<div class="buttons_wrapper"><div class="gallery_index"><div><span class="gallery_index_current gallery_index_current_'.$name_gallery.'">1</span><span class="gallery_index_total gallery_index_total_'.$name_gallery.'"></span></div></div>';
            $output .= '<button class="button_mosaique">Mosaique</button><button data-image="0" class="button_open_fancy">Open fancybox</button></div> ';
            $output .= '<div class="gallery_ajax gallery_ajax_'.$name_gallery.'"></div>';
            $output .= '</div>';
            //var_dump($data_slider);
            return $output;
            //break;
        
    }
        return $output;
    }

add_filter('post_gallery', 'wptester_post_gallery', 10, 2 );