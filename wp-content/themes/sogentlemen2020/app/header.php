<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta name="google-site-verification" content="EhY9F4p6tGiaPsV6XGzONm6OwyPQ-3a-rIsYQIDhGfo">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<!-- FavIcons -->
			<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-icon-180x180.png">
			<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/android-icon-192x192.png">
			<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-32x32.png">
			<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-96x96.png">
			<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-16x16.png">
			<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/manifest.json">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/ms-icon-144x144.png">
			<meta name="theme-color" content="#ffffff">
		<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '2325750500982832'); // Insert your pixel ID here.
			fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=2325750500982832&ev=PageView&noscript=1"
			/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
		<script type="text/javascript">
		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>",
		id_post = "<?php echo get_the_ID(); ?>",
		id_author = "<?php echo get_the_author_meta( 'user_login' ); ?>",
		article_title = "<?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?>",
		article_link = "<?php the_permalink(); ?>",
		article_thumb = "<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>",
		article_query_search = "<?php echo get_search_query(); ?>";
		</script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header id="header" class="header entry-header" role="banner">
			<div class="top-menu">
				<div class="site-inner">
					<div class="top-left">
						<a href="<?php _e( 'http://www.so-gentlemen.be/nl/', 'html5blank' ); ?>" class="lang-switch">
							<?php _e( 'NL', 'html5blank' ); ?></a>
					</div>
					<div class="top-right"><?php wp_nav_menu( array( 'theme_location' => 'top-menu-2018', 'menu_class' => 'top-menu-2018' ) ); ?>
						<a class="search"></a><a class="search-close"></a></div>
				</div>
			</div>
			<div class="top-banner">
				<div class="site-inner">
					<a href="<?php echo home_url(); ?>" class="site-logo sprite"></a>
					<div id="burger-btn"><span></span></div>
					<div class="mag-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'magazines-menu-2018', 'menu_class' => 'magazines-menu-2018' ) ); ?>
					</div>	
					<a class="search"></a><a class="search-close"></a>
				</div>
			</div>	
			<div class="desktop-menu">
				<div class="site-inner">
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu-2018', 'menu_class' => 'main-menu-2018' ) ); ?>
				</div>
			</div>	
		</header>
		<div id="floating-menu">
			<div class="site-inner">
				<div class="top-left">
					<a class="lang-switch">
						<?php _e( 'NL', 'html5blank' ); ?></a>
				</div>
				<a href="<?php echo home_url(); ?>" class="site-logo sprite"></a>
				<div class="top-right">
					<a class="search"></a>
					<a class="search-close"></a>
				</div>
			</div>
		</div>
		<div id="mobile-menu">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu-2018', 'menu_class' => 'main-menu-2018' ) ); ?>
			<div class="mag-intro">- <?php _e( 'Nos magazines', 'html5blank' ); ?> -</div>
			<?php wp_nav_menu( array( 'theme_location' => 'magazines-menu-2018', 'menu_class' => 'magazines-menu-2018' ) ); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'top-menu-2018', 'menu_class' => 'top-menu-2018' ) ); ?>
		</div>
		<style scoped>
			.banner { display:none; margin: 10px auto;}
			@media (min-width: 690px) { .banner { display: block; } }
		</style>
		<a href="https://today.abnamro.be/fr/durabilit%C3%A9?utm_source=ventures&utm_medium=banner" target="_blank" ><img class="banner" src="<?php bloginfo("template_directory"); ?>/img/banner/gent_banner_abn_fr.jpg" alt="ABN BANNER"></a>
		<?php  //gentleman_ad( 'leaderboard' );  ?> 