/*
//////// PHASE 001 > INSTALLATION DES DEPENDANCES
UNE FOIS : installer en global gulp et tous les plugins :
npm install --save-dev gulp-html-replace gulp-inject-string gulp-cssnano gulp-git gulp gulp-rename gulp-sass gulp-htmlhint gulp-csslint gulp-imagemin imagemin-pngquant gulp-notify gulp-cssnano gulp-uglify gulp-htmlmin gulp-autoprefixer gulp-plumber browser-sync -g


Atteindre votre dossier de travail via le terminal et lancer cette ligne pour linker les plugins installés en global:
npm link cssnano gulp-html-replace gulp-inject-string gulp-cssnano gulp gulp-rename gulp-git gulp-sass gulp-htmlhint gulp-csslint gulp-imagemin imagemin-pngquant gulp-notify gulp-cssnano gulp-uglify gulp-htmlmin gulp-autoprefixer gulp-plumber browser-sync
*/

var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var path = require('path');
var autoprefixer = require('gulp-autoprefixer'); 
var htmlReplace = require('gulp-html-replace');
var cssnano = require('gulp-cssnano');

//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
title: 'Gulp',
icon: path.join(__dirname, 'gulp.png')
};

var autoprefixerOpts = {
    overrideBrowserslist: ['last 10 versions', '> 1%']
};

//error notification settings for plumber
var plumberErrorHandler = { errorHandler: notify.onError({
title: notifyInfo.title,
icon: notifyInfo.icon,
message: "Error: <%= error.message %>"
})
};


gulp.task('sass', function(){
return gulp.src('app/scss/*.scss') // Gets all files ending with .scss in app/scss
.pipe(plumber(plumberErrorHandler))
.pipe(sass())
.pipe(cssnano())
.pipe(autoprefixer(autoprefixerOpts)) 
.pipe(gulp.dest('app/css'))
.pipe(browserSync.reload({
stream: true
}))
});


var fs = require("fs");
var inject = require('gulp-inject-string');
// gulp.task('csstoamp', function () {
//     var cssContent = fs.readFileSync("app/css/amp-custom-styles.css", "utf8");
//     gulp.src("app/template-amp-2018.php")
//         .pipe(htmlReplace({   
//         		baseInlineStyles: {
//                 src: gulp.src('app/css/amp-custom-styles.css'),
//                 tpl: '<!-- build:baseInlineStyles --> <style amp-custom>%s</style> <!-- endbuild -->'
//             }
//         }))
//         .pipe(gulp.dest("app"))
//         .pipe(browserSync.reload({
//             stream: true
//     }));
// });
// gulp.task('csstolanding', function () {
//     var cssContent = fs.readFileSync("app/css/styles-landing.css", "utf8");
//     gulp.src("app/landingpage2.php")
//         .pipe(htmlReplace({   
//                 baseInlineStyles: {
//                 src: gulp.src('app/css/styles-landing.css'),
//                 tpl: '<!-- build:baseInlineStyles --> <style type="text/css">%s</style> <!-- endbuild -->'
//             }
//         }))
//         .pipe(gulp.dest("app"))
//         .pipe(browserSync.reload({
//             stream: true
//     }));
// });

var browserSync = require('browser-sync').create();

var watcher = gulp.watch('js/**/*.js' /* You can also pass options and/or a task function here */);
watcher.on('all', function(event, path, stats) {
  console.log('File ' + path + ' was ' + event + ', running tasks...');
});
// OR LISTEN TO INDIVIDUAL EVENT TYPES
watcher.on('change', function(path, stats) {
  console.log('File ' + path + ' was changed, running tasks...');
});

gulp.task('browser',function (){
    browserSync.init([ //Gentil browsersync, focalise-toi sur les fichiers : // .php contenus dans le dossier site,
    ], { // ATTENTION, ON NE PEUT SPECIFIER QUE L'UNE DES OPTIONS SUIVANTES, DONC METTRE CELLE QUE L'ON NE VEUT PAS EN COMMENTAIRE :
    proxy: "http://localhost:8080/sogentleman2018/", //Sinon, balance le tout sur localhost(:8888 < MAC)/site, ADAPTER "site" ET METTRE LE NOM DU DOSSIER DANS LEQUEL WP EST INSTALLE
     injectChanges: true //et injecte les changements css
    });

  gulp.watch('app/scss/**/*.scss', gulp.parallel('sass'))
    .on('change', browserSync.reload);
 
  // Reload when html changes
  gulp.watch(['app/*.html','app/*.php','app/js/**/*.js'])
    .on('change', browserSync.reload);
})

gulp.task('watch', gulp.series('browser'));