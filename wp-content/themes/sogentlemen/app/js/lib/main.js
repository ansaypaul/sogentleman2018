jQuery(document).ready(function(){
		console.log("Lauch fancybox gallery 08012018");
		jQuery( '[data-fancybox]' ).fancybox({
			touch : true,
			baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
						'<div class="fancybox-bg"></div>' +
						'<div class="fancybox-controls"><div class="logo-mobile sprite"></div>' +
							'<div class="fancybox-infobar">' +
								'<div class="fancybox-infobar__body">' +
									'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
								'</div>' +
							'</div>' +
							'<div class="fancybox-back">' +
								'<a href="#">Back to ELLE</a><button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
							'</div>' +
							'<div class="fancybox-buttons">' +
							'</div>' +
						'</div>' +
						'<div class="fancybox-slider-wrap">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
								'<div class="fancybox-slider"></div>' +
						'</div>' +
						'<div class="fancybox-caption-wrap"><div class="caption-top"><div class="logo-desktop sprite"></div><div class="fancybox-caption"></div>'+
						'<div class="addthis_toolbox addthis_default_style addthis_gallery">'+
						'<a class="addthis_button_facebook addthis_32x32_style" style="cursor:pointer"></a>'+
						'<a class="addthis_button_whatsapp addthis_32x32_style" style="cursor:pointer"></a>'+
						'<a class="addthis_button_twitter addthis_32x32_style" style="cursor:pointer"></a>'+
						'<a class="addthis_button_email addthis_32x32_style" style="cursor:pointer"></a>'+
						'</div>'+
						'</div>'+
						'<div class="fancybox-AD caption-bottom"><div id="affiche-box-shop" class="over-halfpage"><div id="affiche-shop" class="affiche">'+
						'<div id="article-shop-1"><div id="AD_IMU"></div>'+
						'</div></div></div></div>'+
						'</div>'+
						'<div class="fancybox-bottom-lb">'+
						'<div class="fancybox-AD caption-bottom-lb"><div id="affiche-box-shop-lb" class=""><div id="affiche-shop-lb" class="affiche">'+
						'<div id="article-shop-lb"><div id="AD_Leaderboard"></div>'+
						'</div></div></div></div>'+
						'</div>'+
						'</div>'+
					'</div>',

				image : {
					protect: false 
				},

				
			beforeMove 	: function( instance, slide ) {
				console.info('Fancybox switch');
				/*ga('send', 'pageview', { 'page': location.pathname + location.search + location.hash});
				ga('send', 'event', 'Gallery', 'show', location.pathname);
	          	ga('newTracker.send', 'pageview', { 'page': location.pathname + location.search + location.hash});
	          	ga('newTracker.send', 'event', 'Gallery', 'show', location.pathname);*/
				//lazyload();			
				var tempsEnMs = Date.now();
				if (!navigator.userAgent.match(/(android|iphone|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook|ipad)/gi)) {
					if(!jQuery('#AD_Leaderboard').attr("loaded")){
						console.log("Desktop LB ads Gal")
						/*jQuery('#AD_Leaderboard').empty();
						jQuery('#AD_Leaderboard').addClass("lazyload");
				    	jQuery('#AD_Leaderboard').attr("loaded",true);
				    	jQuery('#AD_Leaderboard').attr("dateload",tempsEnMs);
						postscribe('#AD_Leaderboard',
									'<div id="pebbleTop" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "elle-fr";  var adheseLocation = "others";  var adheseFormat = "TopLarge"; var adhesePosition = ""; <\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');*/
					}

				if(!jQuery('#AD_IMU').attr("loaded")){
					jQuery('#AD_IMU').empty();
					jQuery('#AD_IMU').addClass("lazyload");
			    	jQuery('#AD_IMU').attr("loaded",true);
			    	jQuery('#AD_IMU').attr("dateload",tempsEnMs);
					postscribe('#AD_IMU',
									'<div id="pebbleTop" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "elle-fr";  var adheseLocation = "others";  var adheseFormat = "MiddleLarge"; <\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');
					}
				}else{
					if(!jQuery('#AD_IMU').attr("loaded")){
					jQuery('#AD_IMU').empty();
					jQuery('#AD_IMU').addClass("lazyload");
			    	jQuery('#AD_IMU').attr("loaded",true);
			    	jQuery('#AD_IMU').attr("dateload",tempsEnMs);
					postscribe('#AD_IMU',
									'<div id="pebbleTop" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "elle-fr";  var adheseLocation = "others";  var adheseFormat = "MOB640x150"; <\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');
					}
				}

				if(slide['type'] == 'inline'){

					/*console.info('SRC : '+ slide['src'] );
					var id_slider_encours = slide['src'].match(/\d+$/)[0]; */
					//console.log("INJECTED ADS NEW index: " + id_slider_encours);
					jQuery('#pebbleMiddle-gallery').empty();
					jQuery('#pebbleMiddle-gallery').addClass("lazyload");
			    	jQuery('#pebbleMiddle-gallery').attr("loaded",true);
			    	jQuery('#pebbleMiddle-gallery').attr("dateload",tempsEnMs);
									postscribe('#pebbleMiddle-gallery',
									'<div id="pebbleMiddle" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "elle-fr";  var adheseLocation = "others";  var adheseFormat = "Middle"; var adhesePosition = "2";<\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');
				}

				//addthis.toolbox('.addthis_toolbox'); 
			},


			  caption : function( instance, item ) {
			    var caption, link, divad, shop_link;

			    if ( item.type === 'image' ) {
			      //shoplink = '<span class="shop-button"><a target="_blank" href="'.jQuery(this).data('shoplink').'">SHOP IT</a></span>';	
			      	shop_link = jQuery(this).data('shoplink');
			      	gallery_link_url = jQuery(this).data('gallery_link_url');
			      	gallery_link_url = 'test';
			     	if(typeof(shop_link) != 'undefined' || (gallery_link_url != '' && typeof(gallery_link_url) != 'undefined')){
			     	 	caption = jQuery(this).data('caption') + '<span class="shop-button"><a target="_blank" href="'+shop_link+'">SHOP IT</a></span>';
			  		}else{
						caption = jQuery(this).data('caption') + 'TEST';
				  	}
			      //caption = '<span class="shop-button"><a target="_blank" href="http://www.google.fr">SHOP IT</a></span>';
			      //shoplink = jQuery(this).data('shoplink');
			      divad   = '' ;
			      return caption + '<br />'+ divad;
			    }

			  },

	});

jQuery('.mygallery').css('opacity','1');
});