var winHeight = $(window).innerHeight();
var winWidth = $(window).innerWidth();

var docHeight = $(document).innerHeight();
var docWidth = $(document).innerWidth();

var scrollTop = $(window).scrollTop();
var device;
var mainOffset = $('header').height();
var var_window = $(window);

/**** FUNCTION GALLERY **********/
function open_fancy(item){
	index_slider_image = $(item).attr( "data-image");
	$(item).parent().parent().find('img.fancy_img:eq('+index_slider_image +')').click();
}

function init_slider_article(){
	/* MOBILE */ 
	console.log('Init Slider Home');
	var swiperArticle = new Swiper('.swiper-container-article', {
			slidesPerView: 1,
					loop: true,
			        pagination: '.swiper-pagination-insta',
			        paginationClickable: true,
			        spaceBetween: 30,
			        passiveListeners:true,
			        pagination: {
	       		 	el: '.swiper-pagination-insta',
	     			},
	     			autoplay: {
					    delay: 3500,
					},
			});		

	if(device == 'sm'){
		var slidesPerView = 1;
	}else{
		var slidesPerView = 3;
	}
			
	var swiperVideo = new Swiper('.swiper-container-video', {
					slidesPerView: slidesPerView,
			        pagination: '.swiper-pagination-insta',
			        paginationClickable: true,
			        spaceBetween: 30,
			        passiveListeners:true,
			        pagination: {
	       		 		el: '.swiper-pagination-insta',
	     			},
	     			loop: false,
				    navigation: {
				      nextEl: '.swiper-button-next',
				      prevEl: '.swiper-button-prev',
				    },
			});		
		}


function init_fancybox(){
		console.log("Lauch fancybox gallery 08012018 AFTER");
		$( '[data-fancybox]' ).fancybox({
			touch : true,
			selector : '',
			baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
						'<div class="fancybox-bg"></div>' +
						'<div class="fancybox-controls"><div class="logo-mobile sprite"></div>' +
							'<div class="fancybox-infobar">' +
								'<div class="fancybox-infobar__body">' +
									'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
								'</div>' +
							'</div>' +
							'<div class="fancybox-back">' +
								'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
							'</div>' +
							'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close fancybox-button--close-right" title="Close (Esc)"></button>'+
							'</div>' +
						'</div>' +
						'<div class="fancybox-slider-wrap">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
								'<div class="fancybox-slider"></div>' +
						'</div>' +
						'<div class="fancybox-caption-wrap"><div class="caption-top"><div class="logo-desktop sprite"></div><div class="fancybox-caption"></div>'+
						'<div id="social-share">'+
						'<a href="https://www.facebook.com/sharer.php?u='+article_link+'" target="_blank" class="facebook sprite sprite-fb"></a>'+
						'<a href="https://twitter.com/intent/tweet?text='+article_title+'&amp;url='+article_link+'&amp;via=Elle.be" target="_blank" class="twitter sprite sprite-twitter"></a>'+
						'<a href="mailto:?subject=ELLE.be | '+article_title+'&amp;body='+article_link+'" class="sprite sprite-mail"></a>'+
						'<a href="whatsapp://send?text='+article_title+' '+article_link+'" target="_blank" class="whatsapp no_desktop sprite sprite-whatsapp"></a>'+
						'<a href="http://pinterest.com/pin/create/bookmarklet/?media='+article_thumb+'&url='+article_link+'&is_video=false&description=ELLE.be | '+article_title+'" target="_blank" class="sprite sprite-pint"></a>'+
						'</div>'+
						'</div>'+
						'<div class="fancybox-AD caption-bottom"><div id="affiche-box-shop" class="over-halfpage"><div id="affiche-shop" class="affiche">'+
						'<div id="article-shop-1">'+
						'<div id="AD_IMU"><div id="halfpage-gallery" class="ads halfpage" categoryAd="" formatMOB="" refadMOB="" format="Middle" refad="pebbleMiddle" location="" position="gallerie"></div></div>'+
						'</div></div></div></div>'+
						'</div>'+
						'<div class="fancybox-bottom-lb">'+
						'<div class="caption-bottom"><div class="fancybox-caption"></div></div>'+
						'<div class="fancybox-AD caption-bottom-lb"><div id="affiche-box-shop-lb" class=""><div id="affiche-shop-lb" class="affiche">'+
						'<div id="article-shop-lb"><div id="AD_Leaderboard">'+
						'<div id="billboard-gallery" class="ads TopLarge" categoryAd="" formatMOB="MOB640x150" refadMOB="pebbleMOB640x150" format="TopLarge" refad="pebbleTopLarge" location="" position="gallerie"></div>'+
						'</div>'+
						'</div></div></div></div>'+
						'</div>'+
						'</div>'+
					'</div>',

				image : {
					protect: false 
				},

				
			beforeMove 	: function( instance, slide ) {
				console.info('Fancybox switch');

				if(slide['type'] == 'inline'){

					/*console.info('SRC : '+ slide['src'] );
					var id_slider_encours = slide['src'].match(/\d+$/)[0]; */
					//console.log("INJECTED ADS NEW index: " + id_slider_encours);
					jQuery('#pebbleMiddle-gallery').empty();
					jQuery('#pebbleMiddle-gallery').addClass("lazyload");
			    	jQuery('#pebbleMiddle-gallery').attr("loaded",true);
			    	jQuery('#pebbleMiddle-gallery').attr("dateload",tempsEnMs);
									postscribe('#pebbleMiddle-gallery',
									'<div id="pebbleMiddle" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "'+lang_identifiantPebble+'";  var adheseLocation = "others";  var adheseFormat = "Middle"; var adhesePosition = "2";<\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');
				}

			},

			afterMove 	: function( instance, slide ) {
				hash = location.hash;
				hash_exp =hash.split("-"); 
				last_object = $( hash_exp[0] ).closest('article');
				last_id = last_object.data("id");
				user_login = last_object.data("author");
				cat = last_object.data("adposition");
				title = last_object.data("title");
				subcat = last_object.data("adpositionbis");
				tag = last_object.data("tag");

				ga('send', 'pageview', {
				'dimension1': user_login,
				'dimension2': cat  + ' (Article)',
				'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
				'dimension4': subcat + ' (Article)',
				'dimension5': tag + ' (Article)'
				});	

				ga('newTracker.send', 'pageview', {
				'dimension1': user_login,
				'dimension2': cat  + ' (Article)',
				'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
				'dimension4': subcat + ' (Article)',
				'dimension5': tag + ' (Article)'
				});	
			},


			  caption : function( instance, item ) {
			    var caption, link, divad, shop_link;

			    if ( item.type === 'image' ) {
			      	if(jQuery(this).parent().parent().parent().find('div.swiper-caption_data').html() != ''){
 						caption = jQuery(this).parent().parent().parent().find('div.swiper-caption_data').html();
			      	}
			    return caption;
			    }

			  },

			  clickContent : function( current, event ) {
				   return current.type === 'image' ? 'zoom' : false;
			  },
	});
}


		var sliders = [];
	    function init_slider_gallery(){  	
	    	$(".gallery_wrapper").each(function(){
			    if(!$(this).attr("loaded")){
		
			      		$(this).attr("loaded",true);

			      		if($(this).hasClass('template-slider')){
	    					format_slider_gallery($(this));
	    				}
	    				else if($(this).hasClass('template-mosaique')){
	    					format_mosaique_gallery($(this));
	    				}
	    				else{
	    					format_slider_gallery($(this));
	    				}

	    			$(this).find('.button_open_fancy').click(function() { open_fancy($(this)); });
					$(this).find('.button_mosaique').click(function() {  
						if($(this).hasClass('active-on')){
							$(this).removeClass('active-on');
							format_slider_gallery($(this).parent().parent()); 
						}else{
							$(this).addClass('active-on');
							format_mosaique_gallery($(this).parent().parent()); 
						}
					});


	    		}
			});
		}	

		function format_slider_gallery(item){  
				current_gallery = $(item).data('id');
				$(item).find("div.gallery_ajax").empty();
	    			$.ajax({
	    						async: false,
								context: this,
								url: stylesheet_directory_uri + '/template-mustache/template.htm',
								type: 'get',
								beforeSend: function() {

								},
								success: function( templates ) {
								$(item).find("div.gallery_ajax").css('opacity','0');
								var template = $(templates).filter('#template-1col').html();	
		    					$(item).find("div.gallery_ajax").html(Mustache.render(template,window["gallery_ajax_" + current_gallery]));

			    					$(item).find( "div.swiper-slide" ).each(function( index ) {
										if(index != 0 && index % 5 == 0){
											console.log('ADD ADS SLIDERS');
											ads_id = Math.floor(Math.random() * 1000000) + 1;
											ads = document.createElement( "div" );
											ads.className  = 'swiper-slide';
											/*ads.innerHTML = '<div class="swiper-picture_wrapper">'+
											'<div id="halfpage-'+ads_id+'" class="ads-slider ads-slider halfpage" categoryAd="" formatMOB="MMR" refadMOB="pebbleMMR" format="Middle" refad="pebbleMiddle" location="" position="2"></div>'+
											'</div>';
											this.after(ads);*/
										}
									});

			    					sliders[current_gallery] = new Swiper($(item).find("div.swiper-container-gallery"), {
															   		pagination: '.swiper-pagination',
															   		slidesPerView: 'auto',
															   		paginationaginationClickable: false,
															   		spaceBetween: 30,
															   		passiveListeners:true,
																    navigation: {
																        nextEl: '.swiper-button-next-galerie',
																        prevEl: '.swiper-button-prev-galerie',

															        },

									});  

			    					sliders[current_gallery].params['ID'] = current_gallery;

									sliders[current_gallery].on('slideChange', function () {
										$(".gallery_index_current_" + this.params['ID']).data('image',this.activeIndex);
										$(".gallery_index_current_" + this.params['ID']).html(this.activeIndex + 1);
										$(item).find(".button_open_fancy").attr('data-image',this.activeIndex);
										dump($(item));
										window.location.hash = '#slider-'+this.params['ID']+'-'+this.activeIndex;

										console.log('Generate page vue - SLIDER');

										last_object = $(item).closest('article');
										last_id = last_object.data("id");
										user_login = last_object.data("author");
										cat = last_object.data("adposition");
										title = last_object.data("title");
										subcat = last_object.data("adpositionbis");
										tag = last_object.data("tag");
										link = last_object.data("link");

										ga('set', 'location', link);
										ga('send', 'pageview', {
										'dimension1': user_login,
										'dimension2': cat  + ' (Article)',
										'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
										'dimension4': subcat + ' (Article)',
										'dimension5': tag + ' (Article)'
										});	

										ga('newTracker.set', 'location', link);
										ga('newTracker.send', 'pageview', {
										'dimension1': user_login,
										'dimension2': cat  + ' (Article)',
										'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
										'dimension4': subcat + ' (Article)',
										'dimension5': tag + ' (Article)'
										});	

									});


								number_slide = $(item).find("div.swiper-slide").length;
								$(".gallery_index_total_" + current_gallery).html(number_slide);

								(item).find('.gallery_index_current').removeClass('hidden');
								(item).find("div.gallery_ajax").fadeTo( "slow", 1 );
					}

			});
	    	lazy_load();
	    	init_fancybox();
		}



		function format_mosaique_gallery(item){  
				current_gallery = $(item).data('id');
				$(item).find("div.gallery_ajax").empty();
	    			$.ajax({
	    						async: false,
								context: this,
								url: stylesheet_directory_uri + '/template-mustache/template.htm',
								type: 'get',
								beforeSend: function() {
						
								},
								success: function( templates ) {
								var template = $(templates).filter('#template-all').html();		
		    					$(item).find("div.gallery_ajax").html(Mustache.render(template,window["gallery_ajax_" + current_gallery]));
								$(item).find("div.grid").css('opacity','0');
							    $(function() {
										// init Masonry
										var $grid = $('.grid').imagesLoaded( function() {
										  // init Masonry after all images have loaded
										  $grid.masonry({
										  				itemSelector: '.grid-item',
										  				gutter: 5,
										  });
										 $(item).find("div.grid").fadeTo( "slow", 1 );
										});

								});
								$(item).find('.gallery_index_current').addClass('hidden');
								$(item).find('.button_mosaique').addClass('active-on');
								number_slide = $(item).find("div.grid-item").length;
								$(".gallery_index_total_" + current_gallery).html(number_slide);

								/*(item).find('.gallery_index_current').removeClass('hidden');
								(item).find("div.gallery_ajax").fadeTo( "slow", 1 );*/
					}

			});
	    	init_fancybox();
		}

	    function lazy_load(){
		   	var wt = $(window).scrollTop();    //* top of the window
		   	var wb = wt + $(window).height();  //* bottom of the window
		    $("picture.lazy source, .lazy-img").each(function(){
			    var ot = $(this).offset().top - 100;  //* top of object (i.e. advertising div)
			    var ob = ot + $(this).height(); //* bottom of object
			    if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			    		//console.log("Picture Loaded");
			      		$(this).attr("loaded",true);
			      		var srcset = $(this).data('srcset');
			      		if(typeof srcset !== typeof undefined){
							var imageDataSource = $(this).data('srcset').toString();
		  					var setImageSource = $(this).attr('srcset', imageDataSource);
			      		}
		  				//$(this).parent().parent().parent().parent().fadeTo(800, 1 )
					}
			});
		}
	


/********* FCT GALLERY ***********/

function what_device(){
	if (winWidth <= 480) {
		device = 'sm';
	} else if (winWidth <= 640) {
		device = 'md';
	} else{
		device = 'lg';
	}
	console.log('Device : ' + device);
}
function goBurger() {
	$('#burger-btn').click(function() {
		$(this).toggleClass('active-on');
		$("#mobile-menu, body").toggleClass('active-on');
	});
}
function show_secondnav() {
	scrollTop = $(document).scrollTop();
	if (device=='md' || device=='lg'){
		if ( var_window.scrollTop() >= mainOffset ) {
		        $('#floating-menu').addClass('active-on');
		        $('.search-form').addClass('light-on');
		        
		} else{
			$('#floating-menu').removeClass('active-on');
			$('.search-form').removeClass('light-on');
		}
	}
}

function goSearch() {
	$('.search, .search-close').click(function() {
		$(".search-form, body, .search, .search-close").toggleClass('active-on');
	});
}

function setGentlekie( e, t, n ) {
		var r = new Date(),
			i = new Date();
		i.setTime( r.getTime() + n * 24 * 60 * 60 * 1e3 );
		document.cookie = e + '=' + encodeURIComponent( t ) + ';expires=' + i.toGMTString() + ';domain=.so-gentlemen.be;path=/';
}

function getGentlekie( e ) {
		var t = document.cookie,
			n, r, i;
		e = e + '=';
		for ( r = 0, c = t.length; r < c; r++ ) {
			i = r + e.length;
			if ( t.substring( r, i ) == e ) {
				cookEnd = t.indexOf( ';', i );
				if ( cookEnd == -1 ) {
					cookEnd = t.length;
				}
				return decodeURIComponent( t.substring( i, cookEnd ) );
			}
		}
		return null;
}

function init_cookie(){
		if ( getGentlekie( 'displayCookieConsent' ) != 'y' ) {
			$( '#cookie-box' ).addClass('active-on' );
			$( '#cookieChoiceDismiss' ).click( function() {
				$( '#cookie-box' ).remove();
				setGentlekie( 'displayCookieConsent', 'y', 365 );
			} );
		
		} 
}

$(document).ready(function(){
	goSearch();
	// backtotop();
	what_device();
	goBurger();
	init_cookie();
	
});

$(document).scroll(function(){
	//backtotopScroll();
	show_secondnav();
});

$(window).resize(function(){
	// reload de la page
	// window.location.href = window.location.href;

});

$(window).load(function(){
	//
	init_slider_article();
	init_slider_gallery();

});

