<?php get_header(); ?>
<main role="main" id="main" class="main main-404">
	<div class="site-inner">
		<div class="content entry-content">
			<h1><?php _e( 'Erreur 404', 'html5blank' ); ?></h1>
			<p>
				<?php _e( "Oops, cette page n'existe pas!", 'html5blank' ); ?>
			</p>
			<p>
				<?php _e( 'La page que vous recherchez n’existe pas', 'html5blank' ); ?>
			</p>
		</div>
	</div>
</main>

<?php get_footer(); ?>