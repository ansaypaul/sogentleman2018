<?php get_header(); ?>
<main role="main" id="main" class="main main-search">
	<div class="site-inner">
		<div class="content entry-content">
			<h1 class="entry-title"><?php printf( __( _e("Votre recherche pour: ").' %s' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
		<?php $archives = get_posts( array('numberposts' => 10, 'paged' => get_query_var('paged'), 's' => $_GET['s'])); ?>.
		<?php
			$current_post = 0;
			foreach ( $archives as $post ) : setup_postdata( $post ); $excluded_ID[] = $post->ID;?>
					<?php if ($current_post % 2 == 0): 
				        get_template_part('template-parts/general/article-left'); 
				    else: 
				        get_template_part('template-parts/general/article-right'); 
				    endif ?>
			<?php endforeach; wp_reset_postdata();?>
			<div class="navigation"><?php posts_nav_link( '  ', __('Publications récentes','html5blank') ,  __('Anciennes publications','html5blank') ); ?></div>

			<?php if(count($archives)==0){ ?>
			 	<p><?php _e("Votre recherche n'a donné aucun résultat"); ?></p>
			<?php } ?>
		</div>
		<?php get_template_part('sidebar'); ?>
	<div>
</main>
<?php get_footer(); ?>