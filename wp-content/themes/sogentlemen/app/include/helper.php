<?php


/*------------------------------------*\
    Disabled Emoji
\*------------------------------------*/
function disable_emojis() {
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
* 
* @param array $plugins 
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
    return array();
    }
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
if ( 'dns-prefetch' == $relation_type ) {
/** This filter is documented in wp-includes/formatting.php */
$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
}

return $urls;
}

/*------------------------------------*\
    Close comments
\*------------------------------------*/
add_filter('comments_open', 'wpc_comments_closed', 10, 2);
function wpc_comments_closed( $open, $post_id ) {
    $post = get_post( $post_id );
    if ('post' == $post->post_type)
    $open = false;
    return $open;
}


/** Check si la personne est un administrateur un editeur **/
function is_admin_onfrontend(){
	global $current_user; // Use global
	if(is_user_logged_in()){
		wp_get_current_user(); // Make sure global is set, if not set it.
		if($current_user->roles[0] == 'administrator'){ return 1; }else{ return 0; }
	}else{
		return 0;
	}
}


/** single.php **/

function getPostViews($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

/* compteur de page vues */
function elle_set_post_views(){
   		$count_key = 'wpb_post_views_count';
   		$postID = get_the_ID();
    	$count = get_post_meta($postID, $count_key, true);
	    if($count=='' || $count == 0){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '1');
	    	}
	    	else{
		    $count++; 
		    	update_post_meta($postID, $count_key, $count);
	    }
}


/** category.php **/

/* + de articles de la même catégorie */
function get_plus_de_article($array_category,$number_article){
	$output = "";
	$posts = get_posts(array('posts_per_page' => $number_article,'category_in'=> array($id_category)));
	$output .= '<div class="swiper-container">';
	$output .= '<div class="swiper-wrapper">';
	$output .= '<div class="swiper-slide">Slide 1</div>';
	$output .= '<div class="swiper-slide">Slide 1</div>';
	foreach ( $posts as $post ) : setup_postdata( $post );
			$output .= '<div class="swiper-slide">Slide 2</div>';
	endforeach; wp_reset_postdata();
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<script>var swiper = new Swiper(".swiper-container");</script>';
	echo $output;
}