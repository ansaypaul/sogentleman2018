<div id="cookie-box">
	<div class="site-inner">
		<div class="btn-close" id="cookieChoiceDismiss"></div>
			<?php _e("En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de cookies. Ces derniers assurent le bon fonctionnement de nos services.", 'html5blank' ); ?>
		</div>
</div>
<?php get_template_part('template-parts/general/searchform'); ?>
<footer id="footer" role="contentinfo">
	<div class="site-inner">
		<?php wp_nav_menu( array( 'theme_location' => 'magazines-menu-2018', 'menu_class' => 'magazines-menu-2018' ) ); ?>
		<a href="<?php echo home_url(); ?>" class="site-logo sprite"></a>
		<?php wp_nav_menu( array( 'theme_location' => 'top-menu-2018', 'menu_class' => 'top-menu-2018' ) ); ?>
	</div>
</footer>
<small><p>© SO GENTLEMEN <?php echo date("Y"); ?> a division of <a href="http://editionventures.be/" target="_blank">Edition Ventures</a></p></small>

<?php wp_footer(); ?>

<!-- <?php if ( is_singular() ) : ?>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5446356b091989f1" async="async"></script>
		<script type="text/javascript" src="http://www.gentleman.be/js/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="http://www.gentleman.be/js/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<script type="text/javascript">
			$( document ).ready( function() {
				$( '.fancybox' ).fancybox();
			} );
			$( '.entry-content a[href$=".bmp"], .entry-content a[href$=".gif"], .entry-content a[href$=".jpg"], .entry-content a[href$=".jpeg"], .entry-content a[href$=".png"], .entry-content a[href$=".BMP"], .entry-content a[href$=".GIF"], .entry-content a[href$=".JPG"], .entry-content a[href$=".JPEG"], .entry-content a[href$=".PNG"]' ).each( function() {
				$( this ).attr( {
					class: 'fancybox',
					rel: 'gallery-<?php the_ID(); ?>',
				} );
			} );
		</script>

<?php endif; ?> -->
	<script>
	window.beOpinionAsyncInit = function() {
	  BeOpinionSDK.init({
	    account: "58e646c4a78ec75fa222d963",
	  });
	  BeOpinionSDK.parse();
	}
	</script>
	<script async type="text/javascript" src="https://widget.beopinion.com/sdk.js"></script>
	</body>
</html>
