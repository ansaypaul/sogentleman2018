<?php global $excluded_ID; ?>
<div class="first-article">
	<?php $posts = get_posts(array('posts_per_page' => 1, 'order' => 'DESC')); ?>
	<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
		<?php 
			$magazine_array = ['Lobby','Gentleman','Play Golf','Play Tennis','Zoute Paper','High Life','Club Grand Place','Event'];
			$list_categorie = get_the_category(); 
			$main_categorie = $list_categorie[0];
			$excluded_ID[] = $post->ID; 
			if(in_array($main_categorie->name, $magazine_array)){
				$main_categorie = $list_categorie[1];
			}
		?> 
		<article class="article-1 first-article-1">
			<div class="post-side-left">
				<a href="<?php the_permalink(); ?>" class="post-thumbnail">
					<?php the_post_thumbnail('firstarticle-2018'); ?>
				</a>
			</div>
			<div class="post-side-right">
				<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category">
					<?php echo $main_categorie->name; ?>
				</a>
				<div class="post-side-block">
			    	<?php if (is_home()) { ?>
			    		<h1>
				        	<a href="<?php the_permalink();?>" class="post-title"><?php the_title(); ?></a>
				        </h1>
				    <?php  } else{ ?>
						<h2>
				        	<a href="<?php the_permalink();?>" class="post-title"><?php the_title(); ?></a>
				        </h2>
			    	<?php } ?>
			    	<a href="<?php the_permalink();?>" class="post-title"><?php the_excerpt(); ?></a>
		    	</div>
		    </div>
		</article>
	<?php endforeach; wp_reset_postdata(); ?>
</div>