<!-- single-sidebar-related -->
<?php global $excludeIDS; global $main_categorie; ?>
<div class="bottom-related">
	<h3><?php _e( 'A lire aussi', 'html5blank' ); ?></h3>
	<?php
		$i = 0; 
		$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );   
		$categories = wp_get_post_categories( $post->ID ); 
		if( $related ) { ?>
			<div>
				<?php 
				foreach( $related as $post ) {
				setup_postdata($post); ?>
					<article id="post-<?php the_ID(); ?>" class="article-related">
						<a href="<?php the_permalink(); ?>" rel="bookmark" >
							<?php the_post_thumbnail('relatedarticle-2018',array('',1,0)); ?>
						</a>
						<h4>
							<a href="<?php echo the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h4>
					</article>
				<?php } ?>
			</div>
		<?php
		wp_reset_postdata(); ?>
	<?php } ?>
</div> 