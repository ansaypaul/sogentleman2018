<!-- most-popular -->
<div class="most-popular">
	<h3><?php _e( 'les + lus', 'html5blank' ); ?></h3>
	<?php 
		$time_most_popular = '30 days ago';
		$posts_popular = get_posts(array('category__not_in' => array( 10 ),'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));
	?>
	<ul class="most-popular-titles" >
	<?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); ?>
		<li>
			<h4><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</li>
	<?php endforeach; wp_reset_postdata(); ?>
	</ul>
</div>
<!-- most-popular -->