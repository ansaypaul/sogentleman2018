<?php get_header(); ?>
<main role="main" id="main" class="main main-category">
	<div class="site-inner">
		<div class="content entry-content">
			<h1>
				<?php single_cat_title(); ?>
			</h1>
			<span class="desc"><?php echo category_description(); ?></span>
			<?php if( $posts ){ ?>
					<?php $current_post = 0; ?>
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post++; ?>
						<?php if ($current_post % 2 == 0): 
					        get_template_part('template-parts/general/article-left'); 
					    else: 
					        get_template_part('template-parts/general/article-right'); 
					    endif ?>
						<?php if ($current_post == 1){ ?> <?php } ?>
						<?php if ($current_post == 3){ ?> 
						<?php } ?> 
						<?php if ($current_post == 8){ ?> 
						<?php } ?> 
					<?php endforeach; wp_reset_postdata(); ?>
				<div class="navigation"><?php posts_nav_link( '  ', __('Publications récentes','html5blank') ,  __('Anciennes publications','html5blank') ); ?></div>
			<?php } else { ?>
				<p><?php _e( "Il n'y a pas de contenu pour le moment.", 'html5blank' ); ?></p>
			<?php } ?>
		</div>
		<?php get_template_part('sidebar'); ?>
	<div>
</main>

<?php get_footer(); ?>