<?php get_header(); ?>
<?php $excluded_ID  = array(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" id="main" class="main main-index">
	<!-- site-inner -->
	<div class="site-inner">
		<?php if($paged == 1){ get_template_part('template-parts/general/first-article'); } ?>
		<div class="articles content entry-content">
		<?php get_template_part('template-parts/general/swiper-article'); ?>
				<?php 
			    	$args = array('posts_per_page'=> 10 , 'post__not_in' => $excluded_ID, 'paged' => $paged);
					$posts = get_posts($args); 
					$current_post = 0;
					foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
						if ($current_post % 2 == 0): 
					        get_template_part('template-parts/general/article-left'); 
					    else: 
					        get_template_part('template-parts/general/article-right'); 
					    endif ?>
						<?php if ($current_post == 1){ ?> <?php } ?>
						<?php if ($current_post == 3){ ?><?php } ?> 
						<?php if ($current_post == 8){ ?><?php } ?> 
				<?php endforeach; wp_reset_postdata(); ?>
			 <div class="navigation"><?php posts_nav_link( '  ', __('Publications récentes','html5blank') ,  __('Anciennes publications','html5blank') ); ?></div>
		</div>
		<?php get_template_part('sidebar'); ?>
	</div>
</main>
<?php get_footer(); ?>