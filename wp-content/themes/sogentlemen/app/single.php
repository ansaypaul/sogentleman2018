<?php get_header(); ?>
<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
if(count($list_categorie) > 1){
	$sous_categorie = $list_categorie[1];
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
?>
<main role="main" id="main" class="main main-single">
	<div class="site-inner">
		<div class="top-wrapper">
			<div class="post-category">
				<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
					<?php echo $main_categorie->name; ?>
				</a>
				<?php if(isset($sous_categorie) && ($main_categorie->cat_ID != $sous_categorie->cat_ID)){ ?>
					<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
				<?php } ?>
			</div>
			<span class="date">	
				<?php $str_update =  __( 'Publié le', 'html5blank' ); ?>
				<?php if(get_the_modified_time('Y-m-j') !=  get_the_time('Y-m-j')){ $str_update = __( 'Mis à jour le', 'html5blank' ); } ?>
					<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php echo $str_update; ?> <?php the_modified_time('j F Y'); ?></span>
					<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
					</span>
			</span>
		</div>
		<?php while (have_posts()) : the_post(); setup_postdata($post); ?>
			<div class="content entry-content">
				<article id="post-<?php the_ID(); ?>">
					<div vocab="http://schema.org/" typeof="Article">
						<header class="entry-header">
							<h1 property="headline">
								<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
							</h1>
							<div id="social-share">
								<span><?php _e( 'Partager sur', 'html5blank' ); ?></span>
								<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="facebook sprite sprite-fb"></a>
								<a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank" class="twitter sprite sprite-twitter"></a>
								<a href="mailto:?subject=SoGentlemen.be | <?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" class="sprite sprite-mail"></a>
								<a href="whatsapp://send?text=<?php the_title(); ?> <?php the_permalink(); ?>" target="_blank" class="whatsapp no_desktop sprite sprite-whatsapp"></a>
								<a href="http://pinterest.com/pin/create/bookmarklet/?media=&url=<?php the_permalink(); ?>&is_video=false&description=SoGentlemen.be | <?php the_title(); ?>" target="_blank" class="sprite sprite-pint"></a>
							</div>
							<?php if(has_post_thumbnail() && $display_thumb ){ ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
									<?php the_post_thumbnail('firstarticle-2018',array('',0,1,1)); ?>
								</a>            
					               <span class="caption-div">
					                 <?php get_the_post_thumbnail_caption(get_the_ID()); ?>
					              </span>
								<a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a>
							<?php } ?>
						</header>
						<div class="entry-content e-content" property="articleBody">
							<?php echo apply_filters( 'the_content', $post->post_content );  ?>
						</div>
						<footer class="entry-footer">
						</footer>
					</div>
					<div class="BeOpinionWidget" data-my-content="0" /></div>
				</article>
				<?php custom_breadcrumbs(); ?>
				<?php
					echo '<h3 class="site-content-title">Laisser un commentaire</h3>';
					comments_template( '', true );
				?>
			</div>
		<?php endwhile; ?>
		<?php get_template_part('sidebar'); ?>
		<?php get_template_part('template-parts/general/article-related-bottom');  ?>
	</div>
</main>
<?php get_footer(); ?>
